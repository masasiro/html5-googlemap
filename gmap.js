var map;
var marker = null;
var idlat;
var idlng;

function $id(id) {
	return document.getElementById(id);
}
function getCircle() {
	var circle = {
		path: google.maps.SymbolPath.CIRCLE,
		scale: 7,
		fillColor: "deepskyblue",
		fillOpacity: 0.6,
		strokeColor: "orange",
		strokeWeight: 1
	};
	return circle;
}
function hereClick(latlng) {
	marker.setMap(null);
	marker = null;
	idlat.innerHTML = "緯度：" + latlng.ob;
	idlng.innerHTML = "経度：" + latlng.pb;
	idlat.style.visibility = "visible";
	idlng.style.visibility = "visible";
}
function hereAdd(latlng) {
	var here	= new google.maps.Marker({map:map, position:latlng, title:"Here", icon:getCircle()});
	google.maps.event.addListener(here, 'click', function(event){hereClick(event.latLng);});
}
function markerClick(latlng) {
	marker.setMap(null);
	marker = null;
	idlat.style.visibility = "hidden";
	idlng.style.visibility = "hidden";
}
function markerAdd(latlng) {
	if( null !== marker )
	{
		marker.setMap(null);
		marker = null;
	}
	marker = new google.maps.Marker({position:latlng, map:map});
	google.maps.event.addListener(marker, 'click', function(event){markerClick(event.latLng);});
	idlat.innerHTML = "緯度：" + latlng.ob;
	idlng.innerHTML = "経度：" + latlng.pb;
	idlat.style.visibility = "visible";
	idlng.style.visibility = "visible";
}
function mapClick(latlng) {
	markerAdd(latlng);
}
function mapGetOptions(latlng) {
	var agent = navigator.userAgent;
	var options;
	
	if ( agent.indexOf("iPhone") !== -1 ) {
		options = {
			center : latlng,
			zoom : 13,
			streetViewControl : false,
			navigationControl : false,
			zoomControl : false,
			mapTypeControl : true,
			scaleControl : true
		};
	} else if( agent.indexOf("iPad") !== -1 ) {
		options = {
			center : latlng,
			zoom : 13,
			streetViewControl : false,
			navigationControl : false,
			zoomControl : false,
			mapTypeControl : true,
			scaleControl : true
		};
	} else if( agent.indexOf("Android") !== -1 ) {
		options = {
			center : latlng,
			zoom : 13,
			streetViewControl : false,
			navigationControl : true,
			zoomControl : true,
			mapTypeControl : true,
			scaleControl : true
		};
	} else {
		options = {
			center : latlng,
			zoom : 13,
			streetViewControl : false,
			navigationControl : true,
			zoomControl : true,
			mapTypeControl : true,
			scaleControl : true
		};
	}
	
	return options;
}
function createGoogleMap(latlng) {
	var options	= {zoom:13, center:latlng, mapTypeId:google.maps.MapTypeId.ROADMAP};
	map			= new google.maps.Map(document.getElementById("map"), mapGetOptions(latlng));
	google.maps.event.addListener(map, 'click', function(event){mapClick(event.latLng);});
}
function geoSuccessCallback(position) {
	var latlng	= new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
	createGoogleMap(latlng);
	hereAdd(latlng);
}
function geoErrorCallback(error) {
	createGoogleMap(35.6811892119205, 139.76722955703735);
	switch(error.code) {
	case error.UNKNOWN_ERROR:
		alert("Can't get your location : Unknown");
		break;
	case error.PERMISSION_DENIED:
		alert("Can't get your location : Permission Denied");
		break;
	case error.POSITION_UNAVAILABLE:
		alert("Can't get your location : Position Unavailable");
		break;
	case error.TIMEOUT:
		alert("Can't get your location : Timeout");
		break;
	default:
		alert("Can't get your location : Unknown : " + error.code);
		break;
	}
}
function getHere() {
	navigator.geolocation.getCurrentPosition(
		geoSuccessCallback,
		geoErrorCallback,
		{maximumAge:600000, timeout:60000});
}
function main() {
	idlat = $id("lat");
	idlng = $id("lng");
	getHere();
}
google.maps.event.addDomListener(window, "load", main);
